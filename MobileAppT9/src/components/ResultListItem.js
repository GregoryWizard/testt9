import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

class ResultListItem extends Component {
  render() {
    const { resultItem } = this.props;
    return (
      <View style={styles.resultItem}>
        <Text style={styles.resultItemText}>
          {resultItem}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  resultItem: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  resultItemText: {
    fontSize: 20,
    margin: 5,
    fontWeight: 'bold',
    color: 'grey',
  }
});

export default ResultListItem;
