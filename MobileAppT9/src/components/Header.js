import React, { Component } from 'react';
import { 
  StyleSheet, 
  View, 
  Text,
  Platform,
} from 'react-native';

class Header extends Component {
  render() {
    return (
      <View style={styles.header}>
        <Text style={styles.headerText}>
          T9
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#62ADD5',
    paddingTop: (Platform.OS === 'ios') ? 20 : 0,
  },
  headerText: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: 'white',
    fontWeight: 'bold',
  },
});

export default Header;
