import React, { Component } from 'react';
import { ListView } from 'react-native';
import { connect } from 'react-redux';

import Item from './ResultListItem';

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

class ResultList extends Component {
  render() {
    const { result } = this.props;
    return (
      <ListView
        dataSource={ds.cloneWithRows(result)}
        renderRow={(rowData) => 
          <Item resultItem={rowData} />
        }
      />
    );
  }
}

const mapStateToProps = state => ({
  result: state.result,
});

export default connect(mapStateToProps)(ResultList);