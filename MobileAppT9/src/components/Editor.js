import React, { Component } from 'react';
import { 
  StyleSheet, 
  View, 
  Text,
  TextInput,
  TouchableOpacity,
  Platform,
} from 'react-native';
import { connect } from 'react-redux';

import { clear, changeNumber } from '../actions/number';

class Editor extends Component {
  onChangeText(value) {
    const re = /^[2-9\b]+$/;
    if (re.test(value)) {
      this.props.changeNumber(value);
    }
    if (!value) {
      this.props.clear();
    }
  }

  onPressClear = () => {
    this.props.clear();
  }

  render() {
    const { number } = this.props;

    return (
      <View style={styles.editor}>
        <View style={ (Platform.OS === 'ios') ? styles.textInputWrapperIOS : styles.textInputWrapperAndroid}>
          <TextInput 
            style={styles.textInput}
            placeholder="Enter the number"
            keyboardType='numeric'
            value={number}
            maxLength={21}
            autoFocus={true}
            onChangeText={value => this.onChangeText(value)}
          />
        </View>
        <TouchableOpacity 
          style={styles.button}
          onPress={this.onPressClear}
        > 
          <Text style={styles.buttonText}>
            Clear
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  editor: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 10,
  },
  textInput: {
    height: (Platform.OS === 'ios') ? 30 : 40,
    padding: 5,
  },
  textInputWrapperIOS: {
    flex: 2,
    marginRight: 10,
    borderBottomWidth: 1, 
  },
  textInputWrapperAndroid: {
    flex: 2,
    marginRight: 10, 
  },
  button: {
    flex: 1,
    backgroundColor: '#62ADD5',
    borderWidth: 1,
    borderColor: '#62ADD5',
    borderRadius: 5,
  },
  buttonText: {
    fontSize: 16,
    padding: 5,
    textAlign: 'center',
    color: 'white',
    fontWeight: 'bold',
  },
});

const mapStateToProps = state => ({
  number: state.number,
});

export default connect(mapStateToProps, { clear, changeNumber })(Editor);
