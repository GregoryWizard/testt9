import axios from 'axios';

// bug android. insert actual url instead localhost
const API_BASE = 'http://localhost:9000/api';
// const API_BASE = 'http://192.168.0.102:9000/api';

export function getTranscription(number) {
  const URL = `${API_BASE}/transcription`;
  return axios.post(URL, {
    number,
  })
    .then(response => response.data);
  console.warn(number)
}

export default {
  getTranscription
};
