import {
  CLEAR,
  CHANGE_NUMBER,
} from '../actions/number';

const number = (state = '', action) => {
  switch (action.type) {
    case CHANGE_NUMBER:
      return action.number;
    case CLEAR:
      return '';
    default:
      return state;
  }
};

export default number;