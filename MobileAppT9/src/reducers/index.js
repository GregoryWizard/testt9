import { combineReducers } from 'redux';

import number from './number';
import result from './result';

export default combineReducers({
  number,
  result,
});