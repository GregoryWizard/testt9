import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from '../store';

import Main from '../pages/Main';

class MobileAppT9 extends Component {
  render() {
    return (
      <Provider store={store}>
        <Main />
      </Provider>
    );
  }
}

export default MobileAppT9;
