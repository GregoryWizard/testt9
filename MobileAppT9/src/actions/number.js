import api from '../api';

export const CLEAR = 'CLEAR';
export const CHANGE_NUMBER = 'CHANGE_NUMBER';

import { returnResult } from './result';

export const clear = () => ({ type: CLEAR });

export const changeNumber = number => (dispatch, getState) => {
  dispatch({  
    type: CHANGE_NUMBER,
    number
  });
  const fullNumber = getState().number;

  return api.getTranscription(fullNumber)
    .then(data => {
      dispatch(returnResult(data))
    })
}