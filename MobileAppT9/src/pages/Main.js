import React, { Component } from 'react';
import { 
  StyleSheet,
  StatusBar,
  View,
} from 'react-native';
import { connect } from 'react-redux';

import Header from '../components/Header';
import Editor from '../components/Editor';
import ResultList from '../components/ResultList';

class Main extends Component {
  render() {
    const { number, result } = this.props;

    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content"/>
        <Header />
        <Editor number={number}/>
        <ResultList result="result"/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: '#F5FCFF',
  },
});

const mapStateToProps = state => ({
  number: state.number,
});

export default connect(mapStateToProps)(Main);
