const apiTranscription = require('./transcription');

module.exports = (express) => {
  const router = express.Router();

  // POST: get transcription (number)
  //curl -H "Content-Type: application/json" -X POST -er": "66639"}' http://localhost:9000/api/transcription
  router.post('/transcription', apiTranscription.getTranscription);

  return router;
};