const fs = require('fs');
let wordList = {};

const keyMap = {
  2 : ['a', 'b', 'c'],
  3 : ['d', 'e', 'f'],
  4 : ['g', 'h', 'i'],
  5 : ['j', 'k', 'l'],
  6 : ['m', 'n', 'o'],
  7 : ['p', 'q', 'r', 's'],
  8 : ['t', 'u', 'v'],
  9 : ['w', 'x', 'y', 'z']
};

const sourceToFile = './google-10000-english.csv';

try {
  const contents = fs.readFileSync(sourceToFile).toString();
  wordList = contents.split('\r\n');
} catch (e){
  console.error(e);
}

getWordsFromLetters = (digit, index, wordLength, wordList) => {
  let match = [];
  for (const letter in keyMap[digit]) {
    for (const word in wordList) {
      if (wordLength == wordList[word].length && keyMap[digit][letter] == wordList[word][index]) {
        match.push(wordList[word]);
      }
    }
  }
  return match;
}


const getTranscription = (req, res) => {
  const { number } = req.body;
  let resultList = wordList;
  const digits = number.toString().split('');
  const wordLength = digits.length;

  for (var i = 0; i < digits.length; i++) {
    resultList = getWordsFromLetters(digits[i], i, wordLength, resultList);
  }

  if ( resultList.length === 'undefined' || resultList.length <= 0) {
    resultList.push(number);
  }

  res.send(resultList);
}


module.exports = {
  getTranscription
};
