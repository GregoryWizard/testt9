Install dependencies:

```sh
npm install
```

Start the project at [`http://localhost:3000`](http://localhost:3000).

```sh
npm start
```

Start server

```sh
npm run start:server
```

Run iOS App

```sh
npm run start:mobile:ios
```

Run Android App

```sh
npm run start:mobile:android
```