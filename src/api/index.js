import axios from 'axios';

const API_BASE = 'http://localhost:9000/api';

export function getTranscription(number) {
  const URL = `${API_BASE}/transcription`;
  return axios.post(URL, {
    number,
  })
    .then(response => response.data);
}

export default {
  getTranscription
};
