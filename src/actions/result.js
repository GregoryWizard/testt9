export const RETURN_RESULT = 'RETURN_RESULT';
export const CLEAR = 'CLEAR';

export const returnResult = result => ({ type: RETURN_RESULT, result });
