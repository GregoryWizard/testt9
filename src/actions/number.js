import api from '../api';

export const CLEAR = 'CLEAR';
export const ADD_NUMBER = 'ADD_NUMBER';
export const DELETE_NUMBER = 'DELETE_NUMBER';

import { returnResult } from './result';

export const clear = () => ({ type: CLEAR });

export const addNumber = number => (dispatch, getState) => {
  dispatch({  
    type: ADD_NUMBER,
    number
  });
  const fullNumber = getState().number;

  return api.getTranscription(fullNumber)
    .then(data => {
      dispatch(returnResult(data))
    })
}

export const deleteNumber = () => (dispatch, getState) => {
  dispatch({  
    type: DELETE_NUMBER,
  });

  const fullNumber = getState().number;

  if (fullNumber !== undefined && fullNumber.length > 0) {
    return api.getTranscription(fullNumber)
    .then(data => {
      dispatch(returnResult(data))
    })
  }  else {
    dispatch(returnResult([]));
  }
}
