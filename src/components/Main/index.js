import React, { Component } from 'react';
import { connect } from 'react-redux';

import './style.css';

import Editor from '../Editor';
import PhonePanel from '../PhonePanel';
import ResultList from '../ResultList';

class Main extends Component {
  render() {
    const { number } = this.props;

    return (
      <div className="Main">
        <div className="Main-header">
          <h2 className="Main-header-text">T9</h2>
        </div>
        <div className="Main-body">
          <div className="Main-left-panel">
            <Editor number={number} />
            <ResultList />
          </div>
          <PhonePanel number={number}/>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  number: state.number,
});

export default connect(mapStateToProps)(Main);
