import React, { Component } from 'react';
import { connect } from 'react-redux';

import './style.css';


class ResultList extends Component {
  render() {
    const { result } = this.props;
    return (
      <div className="List-wrapper">
        {
          result.map((item, key) => {
            return <p className="Item-wrapper" key={key}> {item} </p>
          })
        }
      </div>
    );   
  }
}

const mapStateToProps = state => ({
  result: state.result,
});

export default connect(mapStateToProps)(ResultList);