import React, { Component } from 'react';

import './style.css';

import PhoneButton from '../PhoneButton';

class PhonePanel extends Component {
  render() {
    const fullNumber = this.props.number;

    return (
      <div className="Phone-wrapper"> 
        <div className="Phone-container">
          <PhoneButton number={1} />
          <PhoneButton number={2} />
          <PhoneButton number={3} />
        </div>
        <div className="Phone-container">
          <PhoneButton number={4} />
          <PhoneButton number={5} />
          <PhoneButton number={6} />
        </div>
        <div className="Phone-container">
          <PhoneButton number={7} />
          <PhoneButton number={8} />
          <PhoneButton number={9} />
        </div>
        <div className="Phone-container">
          <PhoneButton />
          <PhoneButton number={0} />
          <PhoneButton type="delete" fullNumber={fullNumber} />
        </div>
      </div>
    );
  }
}

export default PhonePanel;