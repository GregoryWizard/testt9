import React, { Component } from 'react';
import { connect } from 'react-redux';

import './style.css';

import { addNumber, deleteNumber } from '../../actions/number';

const keys = {
  0: [],
  1: [],
  2: ['a','b','c'],
  3: ['d','e','f'],
  4: ['g','h','i'],
  5: ['j','k','l'],
  6: ['m','n','o'],
  7: ['p','q','r','s'],
  8: ['t','u','v'],
  9: ['w','x','y','z'],
};

class PhoneButton extends Component {
  handleOnClick(number) {
    this.props.addNumber(number);
  }

  handleClickDelete = () => {
    this.props.deleteNumber();
  }

  render() {
    const { number, type, fullNumber } = this.props;

    return (
      <div>
        { type==="delete" ?
          <button 
            className="Phone-button" 
            onClick={this.handleClickDelete}
            disabled={fullNumber === ''}
          > 
            <div className="Phone-button-number"> &larr; </div>
          </button>
          :
          <button 
            className="Phone-button" 
            onClick={() => this.handleOnClick(number)}
            disabled={number===undefined || number===0 || number===1}
          > 
            <div className="Phone-button-number"> {number} </div>
            <div className="Phone-button-simbols">
              {keys[number]}
            </div>
          </button>
        }
      </div>
    );
  }
}

export default connect(null, { addNumber, deleteNumber })(PhoneButton);