import React, { Component } from 'react';
import { connect } from 'react-redux';

import './style.css';

import {clear} from '../../actions/number';

class Editor extends Component {
  state = {
    text: ''
  };

  componentWillReceiveProps(nextProps){
    this.setState({text: nextProps.number});
  }

  componentDidMount(){
    this.setState({text: this.props.number});
  }

  handleOnClick = () => {
    this.props.clear();
  }

  render() {
    return (
      <div>
        <div className="Editor-wraper">
          <textarea
            rows={1}
            placeholder="Click key on panel"
            className="Editor-textarea"
            value={this.state.text}
            disabled
          />
          <button 
            className="Editor-button"
            onClick={this.handleOnClick}
          > 
            Clear 
          </button>
        </div>
      </div>  
    );
  }
}
export default connect(null, { clear })(Editor);
