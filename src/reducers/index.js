import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import number from './number';
import result from './result';

export default combineReducers({
  number,
  result,
  routing: routerReducer
});