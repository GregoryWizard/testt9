import {
  CLEAR,
  ADD_NUMBER,
  DELETE_NUMBER
} from '../actions/number';

const number = (state = '', action) => {
  switch (action.type) {
    case ADD_NUMBER:
      return state.concat(action.number);
    case DELETE_NUMBER:
      if (state.number !== '') {
        return state.slice(0, -1);
      } else {
        return state;
      }
    case CLEAR:
      return '';
    default:
      return state;
  }
};

export default number;
