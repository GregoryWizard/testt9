import {
  RETURN_RESULT,
  CLEAR,
} from '../actions/result';

const result = (state = [], action) => {
  switch (action.type) {
    case RETURN_RESULT:
      return action.result;
    case CLEAR:
      return [];
    default:
      return state;
  }
};

export default result;
